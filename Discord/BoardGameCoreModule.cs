using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoardGameNotifier.BoardGames.BoardGameCore;
using BoardGameNotifier.Utilities;
using Discord;
using Discord.Commands;
using Serilog;

namespace BoardGameNotifier.Discord
{
    [Group("bgc")]
    public class BoardGameCoreModule : ModuleBase<SocketCommandContext>
    {
        private const string Service = "bgc";
        private readonly BoardGameWatcher boardGameWatcher;
        private readonly CommandService commandService;
        private readonly ChromeDriverFactory chromeDriverFactory;
        private readonly Options options;

        public BoardGameCoreModule(BoardGameWatcher boardGameWatcher, CommandService commandService, ChromeDriverFactory chromeDriverFactory, Options options)
        {
            this.boardGameWatcher = boardGameWatcher;
            this.commandService = commandService;
            this.chromeDriverFactory = chromeDriverFactory;
            this.options = options;
        }

        [Command("watch")]
        [Summary("Specify a game to watch.")]
        public async Task WatchAsync(
            [Summary("The game type (ie, \"fcm\" for Food Chain Magnate).")]
            string game,
            [Summary("The game ID.")]
            string gameId,
            [Summary("Space separated user mappings (optional).")]
            params string[] userMappings)
        {
            var mappings = new List<Tuple<string, string>>();
            for (int i = 0; i < userMappings.Length; i += 2)
            {
                try
                {
                    mappings.Add(new Tuple<string, string>(userMappings[i], userMappings[i + 1]));
                }
                catch (Exception ex)
                {
                    Log.Debug(ex, "Tried to add uneven amount of users.");
                }
            }

            await ReplyAsync($"Attempting to load game, please be patient...");

            var boardGame = new BGCGame(game, gameId, this.chromeDriverFactory.GetDriver(game + gameId), this.options);

            if (await this.boardGameWatcher.WatchBoardGame(boardGame, Service + game + gameId, mappings, Context))
            {
                var embed = await boardGame.GetBoardGameEmbed();
                await ReplyAsync($"Okay! Watching `{boardGame.GameState.GameName}`!", false, embed);
            }
            else
            {
                await ReplyAsync($"Error fetching game. Make sure the given game exists!");
            }

            await Task.CompletedTask;
        }

        [Command("removeWatch")]
        [Alias("remove")]
        [Summary("Specify a game to stop watching.")]
        public async Task RemoveWatchAsync(
            [Summary("The game type (ie, \"fcm\" for Food Chain Magnate).")]
            string game,
            [Summary("The game ID.")]
            string gameId)
        {
            if (await this.boardGameWatcher.RemoveWatch(Service, game, gameId, Context))
            {
                await ReplyAsync("Game removed from watch list.");
            }
            else
            {
                await ReplyAsync($"Strange, I don't seem to be watching that game... (Check currently watched games with `!{Service} list` command)");
            }
        }

        [Command("list")]
        [Summary("List the games currently watched by this channel.")]
        public async Task ListAsync()
        {
            var boardGames = this.boardGameWatcher.ListBoardGames(Service, Context);
            if (boardGames == null || boardGames.ToList().Count == 0)
            {
                await ReplyAsync($"This channel is not currently watching any {Service} games. Watch games with the `!{Service} watch` command.");
                return;
            }

            var response = "This channel is currently watching: ";
            var embed = await boardGames.GetBoardGamesEmbed();
            await ReplyAsync(response, false, embed);
        }

        [Command("currentPlayers")]
        [Summary("List the current players for the given game (or all games if game not given).")]
        public async Task CurrentPlayersAsync(
            [Summary("The game type (ie, \"fcm\" for Food Chain Magnate). (optional)")]
            string game = null,
            [Summary("The game ID. (optional, required if `game` given)")]
            string gameId = null)
        {
            if (game != null && gameId != null)
            {
                var boardGame = await this.boardGameWatcher.GetBoardGame(Service + game + gameId, Context);
                var embed = await boardGame.GetBoardGameEmbed();
                await ReplyAsync(embed: embed);
            }
            else
            {
                var embed = await this.boardGameWatcher.ListBoardGames(Context).GetBoardGamesEmbed();
                await ReplyAsync("Current players for all games: \n", false, embed);
            }
        }

        [Command("help")]
        [Alias("listcommands")]
        [Summary("Lists the BGN bot's available commands for BoardGameCore.")]
        public async Task ListCommandsAsync()
        {
            var commands = commandService.Commands.Where(command => command.Module.Group == Service);
            var embedBuilder = new EmbedBuilder();

            embedBuilder.WithTitle("BoardGameCore Help");

            foreach (var command in commands)
            {
                var embedFieldName = $"`!{Service} {command.Name}`";
                var embedFieldText = command.Summary + "\n" ?? "No description available.\n";

                if (command.Parameters.Count == 0)
                {
                    embedFieldText += "\tNo parameters.\n";
                }
                else
                {
                    foreach (var parameter in command.Parameters)
                    {
                        var paramSummary = parameter.Summary ?? "No description available.\n";
                        embedFieldText += $"\t`{parameter.Name}`: {parameter.Summary}\n";
                    }
                }

                embedBuilder.AddField(embedFieldName, embedFieldText);
            }

            await ReplyAsync("Here's a list of commands and their descriptions: ", false, embedBuilder.Build());
        }
    }
}
using System;
using System.Reflection;
using System.Threading.Tasks;
using BoardGameNotifier.Utilities;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace BoardGameNotifier.Discord
{
    public class CommandHandler
    {
        private readonly DiscordSocketClient _client;
        private readonly CommandService _commands;
        private readonly IServiceProvider _services;
        private readonly BoardGameWatcher _watcher;

        public CommandHandler(IServiceProvider services, CommandService commands, DiscordSocketClient client, BoardGameWatcher watcher)
        {
            _commands = commands;
            _services = services;
            _client = client;
            _watcher = watcher;
        }

        public async Task InitializeAsync()
        {
            _client.MessageReceived += HandleCommandAsync;
            _client.MessageUpdated += HandleEditAsync;
            _client.LeftGuild += LeftGuild;

            await _commands.AddModulesAsync(assembly: Assembly.GetEntryAssembly(),
                                            services: _services);
        }

        private async Task LeftGuild(SocketGuild server)
        {
            await _watcher.RemoveServer(server);
        }

        private async Task HandleEditAsync(Cacheable<IMessage, ulong> cachedMessage, SocketMessage newMessage, ISocketMessageChannel socketChannel)
        {
            await this.HandleCommandAsync(newMessage);
        }

        private async Task HandleCommandAsync(SocketMessage messageParam)
        {
            // Don't process the command if it was a system message
            var message = messageParam as SocketUserMessage;
            if (message == null) return;

            // Create a number to track where the prefix ends and the command begins
            int argPos = 0;

            // Determine if the message is a command based on the prefix and make sure no bots trigger commands
            if (!(message.HasCharPrefix('!', ref argPos) ||
                message.HasMentionPrefix(_client.CurrentUser, ref argPos)) ||
                message.Author.IsBot)
                return;

            // Create a WebSocket-based command context based on the message
            var context = new SocketCommandContext(_client, message);

            // Execute the command with the command context we just
            // created, along with the service provider for precondition checks.
            await _commands.ExecuteAsync(
                context: context,
                argPos: argPos,
                services: _services);
        }
    }
}
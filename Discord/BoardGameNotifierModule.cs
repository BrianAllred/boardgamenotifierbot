using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoardGameNotifier.Utilities;
using Discord;
using Discord.Commands;
using Serilog;

namespace BoardGameNotifier.Discord
{
    [Group("bgn")]
    public class BoardGameNotifierModule : ModuleBase<SocketCommandContext>
    {
        private BoardGameWatcher boardGameWatcher;
        private CommandService commandService;

        public BoardGameNotifierModule(BoardGameWatcher boardGameWatcher, CommandService commandService)
        {
            this.boardGameWatcher = boardGameWatcher;
            this.commandService = commandService;
        }

        [Command("watch")]
        [Summary("Specify a game to watch.")]
        public async Task WatchAsync(
            [Summary("The game service (ie, \"bgc\" for BoardGameCore).")]
            string service,
            [Summary("The game type (ie, \"fcm\" for Food Chain Magnate).")]
            string game,
            [Summary("The game ID.")]
            string gameId,
            [Summary("A name by which to identify the game. Ignored if service provides game name. (optional, defaults to the value of `game`)")]
            string gameName = null,
            [Summary("Space separated user mappings (optional).")]
            params string[] userMappings)
        {
            var mappings = new List<Tuple<string, string>>();
            for (int i = 0; i < userMappings.Length; i += 2)
            {
                try
                {
                    mappings.Add(new Tuple<string, string>(userMappings[i], userMappings[i + 1]));
                }
                catch (Exception ex)
                {
                    Log.Debug(ex, "Tried to add uneven amount of users.");
                }
            }

            if (await this.boardGameWatcher.WatchBoardGame(service, game, gameId, gameName, mappings, Context))
            {
                var boardGame = await this.boardGameWatcher.GetBoardGame(service + game + gameId, Context);
                var embed = await boardGame.GetBoardGameEmbed();
                await ReplyAsync($"Okay! Watching `{boardGame.GameState.GameName}`!", false, embed);
            }
            else
            {
                await ReplyAsync($"Error fetching game. Make sure the given game exists!");
            }

            await Task.CompletedTask;
        }

        [Command("removeWatch")]
        [Alias("remove")]
        [Summary("Specify a game to stop watching.")]
        public async Task RemoveWatchAsync(
            [Summary("The game service (ie, \"bga\" for BoardGameArena).")]
            string service,
            [Summary("The game type (ie, \"fcm\" for Food Chain Magnate).")]
            string game,
            [Summary("The game ID.")]
            string gameId)
        {
            if (await this.boardGameWatcher.RemoveWatch(service, game, gameId, Context))
            {
                await ReplyAsync("Game removed from watch list.");
            }
            else
            {
                await ReplyAsync("Strange, I don't seem to be watching that game... (Check currently watched games with `!bgn list` command)");
            }
        }

        [Command("list")]
        [Summary("List the games currently watched by this channel.")]
        public async Task ListAsync()
        {
            var boardGames = this.boardGameWatcher.ListBoardGames(Context);
            if (boardGames == null || boardGames.ToList().Count == 0)
            {
                await ReplyAsync("This channel is not currently watching any games. Watch games with the `!bgn watch` command.");
                return;
            }

            var response = "This channel is currently watching: ";
            var embed = await boardGames.GetBoardGamesEmbed();
            await ReplyAsync(response, false, embed);
        }

        [Command("currentPlayers")]
        [Summary("List the current players for the given game (or all games if game not given).")]
        public async Task CurrentPlayersAsync(
            [Summary("The game service (ie, \"bga\" for BoardGameArena. (optional)")]
            string service = null,
            [Summary("The game type (ie, \"fcm\" for Food Chain Magnate). (optional, required if `service` given)")]
            string game = null,
            [Summary("The game ID. (optional, required if `game` and `service` given)")]
            string gameId = null)
        {
            if (service != null && game != null && gameId != null)
            {
                var boardGame = await this.boardGameWatcher.GetBoardGame(service + game + gameId, Context);
                var embed = await boardGame.GetBoardGameEmbed();
                await ReplyAsync(embed: embed);
            }
            else
            {
                var embed = await this.boardGameWatcher.ListBoardGames(Context).GetBoardGamesEmbed();
                await ReplyAsync("Current players for all games: \n", false, embed);
            }
        }

        [Command("help")]
        [Alias("listcommands")]
        [Summary("Lists the BGN bot's available commands.")]
        public async Task ListCommandsAsync()
        {
            var commands = commandService.Commands.Where(command => command.Module.Group == "bgn");
            var embedBuilder = new EmbedBuilder();

            embedBuilder.WithTitle("BoardGameNotifier Help");

            foreach (var command in commands)
            {
                var embedFieldName = $"`!bgn {command.Name}`";
                var embedFieldText = command.Summary + "\n" ?? "No description available.\n";

                if (command.Parameters.Count == 0)
                {
                    embedFieldText += "\tNo parameters.\n";
                }
                else
                {
                    foreach (var parameter in command.Parameters)
                    {
                        var paramSummary = parameter.Summary ?? "No description available.\n";
                        embedFieldText += $"\t`{parameter.Name}`: {parameter.Summary}\n";
                    }
                }

                embedBuilder.AddField(embedFieldName, embedFieldText);
            }

            embedBuilder.AddField("Service Specific Commands", "Use `!bgc help` or `!bga help` for commands specific to those services.");

            await ReplyAsync("Here's a list of commands and their descriptions: ", false, embedBuilder.Build());
        }
    }
}
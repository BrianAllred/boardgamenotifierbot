FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . ./
RUN dotnet publish -c Release -o out

# Build runtime image
FROM selenium/standalone-chrome
WORKDIR /app
COPY --from=build-env /app/out .

ADD https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb microsoft.deb
RUN sudo apt-get update && sudo apt-get install -y ./microsoft.deb && sudo rm -rf ./microsoft.deb
RUN sudo apt-get update && sudo apt-get install -y aspnetcore-runtime-3.1

ENTRYPOINT ["dotnet", "BoardGameNotifier.dll"]
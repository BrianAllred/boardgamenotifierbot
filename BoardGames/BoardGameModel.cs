namespace BoardGameNotifier.BoardGames
{
    public abstract class BoardGameModel
    {
        public long GameId { get; }

        public string GameName { get; }

        public string Phase { get; }

        public string[] CurrentPlayers { get; }

        public string[] Winners { get; }

        public BoardGameModel(long gameId, string gameName, string phase, string[] currentPlayers, string[] winners)
        {
            this.GameId = gameId;
            this.GameName = gameName;
            this.Phase = phase;
            this.CurrentPlayers = currentPlayers;
            this.Winners = winners;
        }
    }
}
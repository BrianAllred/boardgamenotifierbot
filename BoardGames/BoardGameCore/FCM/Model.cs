using System;
using OpenQA.Selenium;

namespace BoardGameNotifier.BoardGames.BoardGameCore.FCM
{
    public class Model : BoardGameModel
    {
        public Model(long gameId, string gameName, string phase, string[] currentPlayers, string[] winners) : base(gameId, gameName, phase, currentPlayers, winners)
        { }

        public Model(string gameStateScript, string phase, IWebElement winnersDiv) : base(
            long.Parse(ParseVarFromScript("global.gameId", gameStateScript)),
            ParseVarFromScript("global.gameName", gameStateScript, true),
            phase,
            ParseVarFromScript("global.currentPlayers", gameStateScript, true).Split(','),
            ParseWinners(winnersDiv)
        )
        { }

        private static string[] ParseWinners(IWebElement winnersDiv)
        {
            if (winnersDiv.Text.Contains("winner"))
            {
                return new[] { winnersDiv.Text.Split(':')[1].Split('<')[0].Trim() };
            }

            return null;
        }

        private static string ParseVarFromScript(string varName, string script, bool stripQuotes = false)
        {
            var splitScript = script.Split('\n', StringSplitOptions.RemoveEmptyEntries);

            foreach (var line in splitScript)
            {
                var trimmedLine = line.Trim();
                if (trimmedLine.StartsWith(varName))
                {
                    var value = trimmedLine.Split('=', StringSplitOptions.RemoveEmptyEntries)[1].Trim().Split(';', StringSplitOptions.RemoveEmptyEntries)[0];
                    if (stripQuotes)
                    {
                        value = value.Substring(1, value.Length - 2);
                    }

                    return value;
                }
            }

            return null;
        }
    }
}
using System;
using System.Threading.Tasks;
using BoardGameNotifier.Utilities;
using Flurl;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Serilog;

namespace BoardGameNotifier.BoardGames.BoardGameCore
{
    public class BGCGame : BoardGame
    {
        private const string baseUrl = @"http://play.boardgamecore.net/";

        private readonly Options options;

        public override string Service { get; set; } = "bgc";

        public override string FullUrl => Url.Combine(baseUrl, Game, GameId);

        public BGCGame(string game, string gameId, ChromeDriver driver, Options options) : base(game, gameId, driver)
        {
            this.options = options;
        }

        internal override async Task<BoardGameModel> GetGameState()
        {
            try
            {
                await Task.Run(() =>
                {
                    lock (this.driverLock)
                    {
                        this.driver.Navigate().GoToUrl(this.FullUrl);
                        var source = this.driver.PageSource;
                        if (source.ToLower().Contains("error"))
                        {
                            if (string.IsNullOrWhiteSpace(this.options?.BGCUsername) || string.IsNullOrWhiteSpace(this.options?.BGCPassword))
                            {
                                throw new Exception("Authentication required but not given.");
                            }

                            this.LogIn();
                            this.driver.Navigate().GoToUrl(this.FullUrl);
                            source = this.driver.PageSource;
                        }

                        switch (this.Game)
                        {
                            case "fcm":
                                var fcmWinnersDiv = this.driver.FindElementById("actions");
                                var phase = this.driver.FindElementById("phase").Text;
                                var gameStateScript = source.Substring(source.IndexOf("<script>"), source.IndexOf("</script>") - source.IndexOf("<script>"));
                                this.gameState = new FCM.Model(gameStateScript, phase, fcmWinnersDiv);
                                break;
                            case "tgz":
                                var playersMini = this.driver.FindElementById("playersMini");
                                var innerDivs = playersMini.FindElements(By.TagName("div"));
                                var tgzWinnersDiv = this.driver.FindElementById("actions");
                                this.gameState = new TGZ.Model(innerDivs[0], innerDivs[1], tgzWinnersDiv);
                                break;
                        }
                    }
                });

                return this.gameState;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error retrieving game state");
            }

            return this.gameState;
        }

        private void LogIn()
        {
            this.driver.Navigate().GoToUrl(baseUrl);
            this.driver.FindElementByName("login").SendKeys(this.options.BGCUsername);
            this.driver.FindElementByName("password").SendKeys(this.options.BGCPassword);
            this.driver.FindElementByClassName("btn").Click();
        }
    }
}
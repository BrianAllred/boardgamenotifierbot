using OpenQA.Selenium;

namespace BoardGameNotifier.BoardGames.BoardGameCore.TGZ
{
    public class Model : BoardGameModel
    {
        public Model(long gameId, string gameName, string phase, string[] currentPlayers, string[] winners) : base(gameId, gameName, phase, currentPlayers, winners)
        { }

        public Model(IWebElement gameIdNameDiv, IWebElement phasePlayerDiv, IWebElement winnersDiv) : base(
            long.Parse(ExtractGameId(gameIdNameDiv)),
            ExtractGameName(gameIdNameDiv),
            ExtractPhase(phasePlayerDiv),
            ExtractCurrentPlayers(phasePlayerDiv),
            ExtractWinners(winnersDiv)
        )
        { }

        private static string ExtractGameId(IWebElement div)
        {
            var text = div.Text.Split('-')[0].Trim();
            return text.Substring(text.IndexOf(' ')).Trim();
        }

        private static string ExtractGameName(IWebElement div)
        {
            return div.Text.Split('-')[1].Trim();
        }

        private static string ExtractPhase(IWebElement div)
        {
            return div.Text.Split('|')[1].Split(':')[1].Trim();
        }

        private static string[] ExtractCurrentPlayers(IWebElement div)
        {
            return new[] { div.Text.Split('|')[2].Split(':')[1].Trim() };
        }

        private static string[] ExtractWinners(IWebElement div)
        {
            if (div.Text.Contains("winner"))
            {
                return new[] { div.Text.Split(':')[1].Split('<')[0].Trim() };
            }

            return null;
        }
    }
}
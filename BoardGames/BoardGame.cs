using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoardGameNotifier.BoardGames.BoardGameArena;
using BoardGameNotifier.BoardGames.BoardGameCore;
using BoardGameNotifier.Utilities;
using OpenQA.Selenium.Chrome;

namespace BoardGameNotifier.BoardGames
{
    public abstract class BoardGame : IDisposable
    {
        private protected BoardGameModel gameState;

        private protected readonly ChromeDriver driver;

        private protected readonly object driverLock = new object();

        public string Game { get; set; }

        public string GameId { get; set; }

        public abstract string Service { get; set; }

        public abstract string FullUrl { get; }

        public List<Tuple<string, string>> UsernameMappings { get; set; } = new List<Tuple<string, string>>();

        public BoardGameModel GameState
        {
            get
            {
                if (this.gameState == null)
                {
                    try
                    {
                        this.gameState = this.GetGameState().Result;
                        return this.gameState;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }

                return this.gameState;
            }
        }

        public BoardGame(string game, string gameId, ChromeDriver driver)
        {
            this.Game = game;
            this.GameId = gameId;
            this.driver = driver;
        }

        public BoardGame WithUsernameMapping(Tuple<string, string> mapping)
        {
            this.UsernameMappings.Add(mapping);
            return this;
        }

        public BoardGame WithUsernameMapping(string discordName, string boardGameName)
        {
            return this.WithUsernameMapping((discordName, boardGameName));
        }

        public BoardGame WithUsernameMapping((string discordName, string boardGameName) p)
        {
            return this.WithUsernameMapping(new Tuple<string, string>(p.discordName, p.boardGameName));
        }

        public BoardGame WithUsernameMappings(IEnumerable<Tuple<string, string>> mappings)
        {
            this.UsernameMappings.AddRange(mappings);
            return this;
        }

        public async Task<string[]> GetCurrentPlayers()
        {
            var gameState = await this.GetGameState();
            return gameState.CurrentPlayers;
        }

        public string GetUserMapping(string user)
        {
            foreach (var mapping in this.UsernameMappings)
            {
                if (mapping.Item1.Equals(user))
                {
                    return mapping.Item2;
                }

                if (mapping.Item2.Equals(user))
                {
                    return mapping.Item1;
                }
            }

            return null;
        }

        internal abstract Task<BoardGameModel> GetGameState();

        public void Dispose()
        {
            lock (this.driverLock)
            {
                this.driver.Dispose();
            }
        }

        public static BoardGame Create(BoardGameWatchModel model, ChromeDriver driver, Options options = null)
        {
            BoardGame game = null;
            switch (model.Service)
            {
                case "bga":
                    game = new BGAGame(model.Game, model.GameId, driver, model.GameName);
                    break;
                case "bgc":
                    game = new BGCGame(model.Game, model.GameId, driver, options);
                    break;
            }

            return game.WithUsernameMappings(model.UsernameMappings);
        }
    }
}
using System;
using System.Collections.Generic;
using MongoDB.Bson;

namespace BoardGameNotifier.BoardGames
{
    public class BoardGameWatchModel
    {
        public ObjectId Id { get; set; } = ObjectId.GenerateNewId();

        public string BGKey { get; set; }

        public string Service { get; set; }

        public string Game { get; set; }

        public string GameId { get; set; }

        public string GameName { get; set; }

        public ulong ServerId { get; set; }

        public ulong ChannelId { get; set; }

        public string[] CurrentPlayers { get; set; }

        public int ErrorCount { get; set; }

        public List<Tuple<string, string>> UsernameMappings { get; set; }

        public BoardGameWatchModel(string bgKey, string service, string game, string gameId, string gameName, ulong serverId, ulong channelId, string[] currentPlayers, int errorCount, List<Tuple<string, string>> mappings)
        {
            this.BGKey = bgKey;
            this.Service = service;
            this.Game = game;
            this.GameId = gameId;
            this.GameName = gameName;
            this.ServerId = serverId;
            this.ChannelId = channelId;
            this.CurrentPlayers = currentPlayers;
            this.ErrorCount = errorCount;
            this.UsernameMappings = mappings;
        }

        public BoardGameWatchModel(string service, string game, string gameId, string gameName, ulong serverId, ulong channelId, string[] currentPlayers, int errorCount, List<Tuple<string, string>> mappings) : this(
                                    service + game + gameId,
                                    service,
                                    game,
                                    gameId,
                                    gameName,
                                    serverId,
                                    channelId,
                                    currentPlayers,
                                    errorCount,
                                    mappings
                                    )
        { }

        public BoardGameWatchModel(BoardGame boardGame, ulong serverId, ulong channelId) : this(
                                    boardGame.Service,
                                    boardGame.Game,
                                    boardGame.GameId,
                                    boardGame.GameState.GameName,
                                    serverId,
                                    channelId,
                                    boardGame.GameState.CurrentPlayers,
                                    0,
                                    boardGame.UsernameMappings
                                )
        { }
    }
}
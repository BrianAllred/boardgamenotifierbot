using BoardGameNotifier.Utilities;

namespace BoardGameNotifier.BoardGames.BoardGameArena.Keyflower
{
    public class Model : BoardGameModel
    {
        public Model(long gameId, string gameName, string phase, string[] currentPlayers, string[] winners) : base(gameId, gameName, phase, currentPlayers, winners)
        { }

        public static string ExtractPlayers(string json)
        {
            var startIndex = json.IndexOf("\"players") + 10;
            var endIndex = json.MatchingBraceIndex(startIndex) + 1;

            if (endIndex < 0)
            {
                return null;
            }

            return "{\"players\":" + json.Substring(startIndex, endIndex - startIndex) + "}";
        }

        public static string ExtractGameState(string json)
        {
            var startIndex = json.IndexOf("\"gamestate") + 12;
            var endIndex = json.MatchingBraceIndex(startIndex) + 1;

            if (endIndex < 0)
            {
                return null;
            }

            return json.Substring(startIndex, endIndex - startIndex);
        }
    }
}
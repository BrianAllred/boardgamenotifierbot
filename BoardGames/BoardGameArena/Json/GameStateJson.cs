using Newtonsoft.Json;

namespace BoardGameNotifier.BoardGames.BoardGameArena.Json
{
    public class GameStateJson
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("active_player")]
        public long ActivePlayer { get; set; }
    }
}
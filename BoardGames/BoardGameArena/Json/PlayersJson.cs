using System.Collections.Generic;
using Newtonsoft.Json;

namespace BoardGameNotifier.BoardGames.BoardGameArena.Json
{
    public class PlayersJson
    {
        [JsonProperty("players")]
        public Dictionary<string, Player> Players { get; set; }
    }

    public class Player
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("score")]
        public long Score { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }

        [JsonProperty("color_back")]
        public string ColorBack { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("zombie")]
        public long Zombie { get; set; }

        [JsonProperty("eliminated")]
        public long Eliminated { get; set; }

        [JsonProperty("is_ai")]
        public long IsAi { get; set; }

        [JsonProperty("beginner")]
        public bool Beginner { get; set; }
    }
}
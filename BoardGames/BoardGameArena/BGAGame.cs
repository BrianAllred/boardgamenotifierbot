using System;
using System.Linq;
using System.Threading.Tasks;
using BoardGameNotifier.BoardGames.BoardGameArena.Json;
using Flurl;
using Newtonsoft.Json;
using OpenQA.Selenium.Chrome;
using Serilog;

namespace BoardGameNotifier.BoardGames.BoardGameArena
{
    public class BGAGame : BoardGame
    {
        private const string baseUrl = @"https://boardgamearena.com/1/";

        [JsonProperty]
        private readonly string gameName;

        public override string Service { get; set; } = "bga";

        public override string FullUrl => Url.Combine(baseUrl, Game).SetQueryParam("table", GameId);

        public BGAGame(string game, string gameId, ChromeDriver driver, string gameName = null) : base(game, gameId, driver)
        {
            this.gameName = gameName;
        }

        internal override async Task<BoardGameModel> GetGameState()
        {
            try
            {
                await Task.Run(() =>
                {
                    lock (this.driverLock)
                    {
                        this.driver.Navigate().GoToUrl(this.FullUrl);
                        var source = this.driver.PageSource;

                        switch (this.Game)
                        {
                            case "keyflower":
                                var globalStateScript = source.Substring(source.IndexOf("completesetup"), source.IndexOf("</script>", source.IndexOf("completesetup")) - source.IndexOf("completesetup"));

                                var playersJson = JsonConvert.DeserializeObject<PlayersJson>(Keyflower.Model.ExtractPlayers(globalStateScript));
                                var gameStateJson = JsonConvert.DeserializeObject<GameStateJson>(Keyflower.Model.ExtractGameState(globalStateScript));
                                var currentPlayer = playersJson.Players.First(playerPair => playerPair.Key == gameStateJson.ActivePlayer.ToString()).Value.Name;
                                var phase = "Move #" + this.driver.FindElementById("move_nbr").Text;

                                this.gameState = new Keyflower.Model(long.Parse(this.GameId), this.gameName ?? this.Game, phase, new[] { currentPlayer }, null);
                                break;
                        }
                    }
                });

                return this.gameState;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error retrieving game state");
            }

            return this.gameState;
        }
    }
}
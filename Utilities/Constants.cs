namespace BoardGameNotifier.Utilities
{
    public static class Constants
    {
        public const string LogTemplate = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}{NewLine}{Properties:j}{NewLine}";

        public const string FilePath = "/var/BGNBot/servers.json";

        public const string AvatarPath = "Assets/avatar.png";
    }
}
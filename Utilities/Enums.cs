namespace BoardGameNotifier.Utilities
{
    public static class Enums
    {
        public enum LogLevel
        {
            Verbose,
            Debug,
            Information,
            Warning,
            Error,
            Fatal
        }

        public enum EnvVar
        {
            BGN_BOT_TOKEN,
            BGN_POLL_INTERVAL,
            BGN_LOG_LEVEL,
            BGN_BGC_USERNAME,
            BGN_BGC_PASSWORD,
            BGN_MONGO_SERVER,
            BGN_MONGO_PORT,
            BGN_MONGO_DATABASE,
            BGN_MONGO_USERNAME,
            BGN_MONGO_PASSWORD
        }
    }
}
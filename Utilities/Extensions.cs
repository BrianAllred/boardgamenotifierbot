using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoardGameNotifier.BoardGames;
using Discord;

namespace BoardGameNotifier.Utilities
{
    public static class Extensions
    {
        public static async Task<Embed> GetBoardGamesEmbed(this BoardGame[] boardGames)
        {
            var embedBuilder = new EmbedBuilder();
            foreach (var boardGame in boardGames)
            {
                var players = await boardGame.GetCurrentPlayers();
                var winners = boardGame.GameState.Winners;

                if (winners != null && winners.Length > 0)
                {
                    embedBuilder.AddField(boardGame.GameState.GameName, $"Service: {boardGame.Service}\nGame: {boardGame.Game}\nID: {boardGame.GameId}\nWinners: {string.Join(", ", winners)}\n[Click here to review game]({boardGame.FullUrl})");
                }
                else
                {
                    embedBuilder.AddField(boardGame.GameState.GameName, $"Service: {boardGame.Service}\nGame: {boardGame.Game}\nID: {boardGame.GameId}\nCurrent players: {string.Join(", ", players)}\nPhase: {boardGame.GameState.Phase}\n[Click here to play]({boardGame.FullUrl})");
                }
            }

            return embedBuilder.Build();
        }

        public static async Task<Embed> GetBoardGamesEmbed(this IEnumerable<BoardGame> boardGames)
        {
            return await boardGames.ToArray().GetBoardGamesEmbed();
        }

        public static async Task<Embed> GetBoardGameEmbed(this BoardGame boardGame)
        {
            return await new BoardGame[] { boardGame }.GetBoardGamesEmbed();
        }
    }
}
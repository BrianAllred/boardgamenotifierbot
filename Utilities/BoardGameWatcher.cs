using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using BoardGameNotifier.BoardGames;
using BoardGameNotifier.BoardGames.BoardGameArena;
using BoardGameNotifier.BoardGames.BoardGameCore;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using MongoDB.Bson;
using MongoDB.Driver;
using Serilog;

namespace BoardGameNotifier.Utilities
{
    public class BoardGameWatcher
    {
        private Timer watchTimer;

        private IMongoCollection<BoardGameWatchModel> watchCollection => this.mongoClient.GetDatabase(this.options.MongoDatabase).GetCollection<BoardGameWatchModel>("watches");

        private readonly DiscordSocketClient client;

        private readonly MongoClient mongoClient;

        private readonly ChromeDriverFactory chromeDriverFactory;

        private readonly Options options;

        public BoardGameWatcher(DiscordSocketClient client, MongoClient mongoClient, ChromeDriverFactory chromeDriverFactory, Options options)
        {
            this.client = client;
            this.mongoClient = mongoClient;
            this.chromeDriverFactory = chromeDriverFactory;
            this.options = options;
        }

        public async Task<bool> WatchBoardGame(string service, string game, string gameId, string gameName, IEnumerable<Tuple<string, string>> userMappings, SocketCommandContext context)
        {
            BoardGame boardGame;
            var boardGameKey = service + game + gameId;

            switch (service)
            {
                case "bga":
                    boardGame = new BGAGame(game, gameId, this.chromeDriverFactory.GetDriver(game + gameId), gameName);
                    break;
                case "bgc":
                default:
                    boardGame = new BGCGame(game, gameId, this.chromeDriverFactory.GetDriver(game + gameId), this.options);
                    break;
            }

            return await this.WatchBoardGame(boardGame, service + game + gameId, userMappings, context);
        }

        public async Task<bool> WatchBoardGame(BoardGame boardGame, string boardGameKey, IEnumerable<Tuple<string, string>> userMappings, SocketCommandContext context)
        {
            return await this.WatchBoardGame(boardGame, boardGameKey, userMappings, context.Guild.Id, context.Channel.Id);
        }

        public async Task<bool> WatchBoardGame(BoardGame boardGame, string boardGameKey, IEnumerable<Tuple<string, string>> userMappings, ulong serverId, ulong channelId)
        {
            try
            {
                Log.Debug("Getting game state");
                var gameState = boardGame.GameState;
                if (gameState == null)
                {
                    return false;
                }

                if (userMappings != null)
                {
                    boardGame = boardGame.WithUsernameMappings(userMappings);
                }

                var watchModel = new BoardGameWatchModel(boardGame, serverId, channelId);
                var filter = Builders<BoardGameWatchModel>.Filter.And(
                    Builders<BoardGameWatchModel>.Filter.Eq(x => x.BGKey, watchModel.BGKey),
                    Builders<BoardGameWatchModel>.Filter.Eq(x => x.ServerId, serverId),
                    Builders<BoardGameWatchModel>.Filter.Eq(x => x.ChannelId, channelId));

                await this.watchCollection.ReplaceOneAsync(filter, watchModel, new ReplaceOptions { IsUpsert = true });

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<BoardGame> GetBoardGame(string bgKey, SocketCommandContext context)
        {
            try
            {
                var watch = (await this.watchCollection.Find(new BsonDocument()).ToListAsync()).Where(x =>
                            x.ServerId == context.Guild.Id &&
                            x.ChannelId == context.Channel.Id &&
                            x.BGKey == bgKey).FirstOrDefault();

                if (watch != null)
                {
                    return BoardGame.Create(watch, this.chromeDriverFactory.GetDriver(watch.Game + watch.GameId), this.options);
                }
            }
            catch (Exception)
            { }

            return null;
        }

        public IEnumerable<BoardGame> ListBoardGames(SocketCommandContext context)
        {
            try
            {
                var watches = this.watchCollection.AsQueryable().Where(watch =>
                    watch.ChannelId == context.Channel.Id &&
                    watch.ServerId == context.Guild.Id
                );

                var games = watches.ToList().Select(watch => BoardGame.Create(watch, this.chromeDriverFactory.GetDriver(watch.Game + watch.GameId), this.options));

                return games;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Couldn't list board games");
                return null;
            }
        }

        public IEnumerable<BoardGame> ListBoardGames(string service, SocketCommandContext context)
        {
            try
            {
                var watches = this.watchCollection.AsQueryable().Where(watch =>
                   watch.ChannelId == context.Channel.Id &&
                   watch.ServerId == context.Guild.Id &&
                   watch.Service == service
                );

                return watches.ToList().Select(watch => BoardGame.Create(watch, this.chromeDriverFactory.GetDriver(watch.Game + watch.GameId), this.options));
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<bool> RemoveWatch(string service, string game, string gameId, SocketCommandContext context)
        {
            return await this.RemoveWatch(service, game, gameId, context.Guild.Id, context.Channel.Id);
        }

        public async Task<bool> RemoveWatch(string service, string game, string gameId, ulong serverId, ulong channelId)
        {
            try
            {
                var bgKey = service + game + gameId;

                var deleteFilter = Builders<BoardGameWatchModel>.Filter.And(
                    Builders<BoardGameWatchModel>.Filter.Eq(x => x.BGKey, bgKey),
                    Builders<BoardGameWatchModel>.Filter.Eq(x => x.ServerId, serverId),
                    Builders<BoardGameWatchModel>.Filter.Eq(x => x.ChannelId, channelId)
                );

                var deleteResult = await this.watchCollection.DeleteOneAsync(deleteFilter);

                var countFilter = Builders<BoardGameWatchModel>.Filter.And(
                    Builders<BoardGameWatchModel>.Filter.Eq(x => x.Game, game),
                    Builders<BoardGameWatchModel>.Filter.Eq(x => x.GameId, gameId)
                );

                var count = await this.watchCollection.CountDocumentsAsync(countFilter);
                if (count == 0)
                {
                    this.chromeDriverFactory.RemoveDriver(game + gameId);
                }

                return deleteResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error deleting watch");
            }

            return false;
        }

        public async Task RemoveServer(SocketGuild server)
        {
            var filter = Builders<BoardGameWatchModel>.Filter.Eq(x => x.ServerId, server.Id);
            await this.watchCollection.DeleteManyAsync(filter);
        }

        public void SetTimerInterval(int interval)
        {
            if (this.watchTimer != null)
            {
                this.watchTimer.Enabled = false;
            }

            this.watchTimer = new Timer(interval);
            this.watchTimer.Elapsed += (sender, args) => { this.Notify().Wait(); };
            this.watchTimer.AutoReset = false;
            this.watchTimer.Enabled = true;
        }

        private async Task Notify()
        {
            try
            {
                await this.watchCollection.Find(new BsonDocument()).ForEachAsync(async (watch) =>
                {
                    var boardGame = BoardGame.Create(watch, this.chromeDriverFactory.GetDriver(watch.Game + watch.GameId), this.options);
                    var channelSocket = this.client.GetChannel(watch.ChannelId) as ISocketMessageChannel;

                    string[] winners = { };
                    string[] oldCurrentPlayers = { };
                    string[] newCurrentPlayers = { };

                    try
                    {
                        oldCurrentPlayers = watch.CurrentPlayers;
                        newCurrentPlayers = await boardGame.GetCurrentPlayers();
                        winners = boardGame.GameState.Winners;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Error retrieving game state.");
                        watch.ErrorCount++;

                        if (watch.ErrorCount >= 5)
                        {
                            Log.Information("Watch has triggered 5 or more errors, removing from watches.");
                            await this.RemoveWatch(watch.Service, watch.Game, watch.GameId, watch.ServerId, watch.ChannelId);
                        }
                        else
                        {
                            var filter = Builders<BoardGameWatchModel>.Filter.And(
                            Builders<BoardGameWatchModel>.Filter.Eq(x => x.BGKey, watch.BGKey),
                            Builders<BoardGameWatchModel>.Filter.Eq(x => x.ServerId, watch.ServerId),
                            Builders<BoardGameWatchModel>.Filter.Eq(x => x.ChannelId, watch.ChannelId));

                            await this.watchCollection.ReplaceOneAsync(filter, watch, new ReplaceOptions { IsUpsert = true });
                        }

                        return;
                    }

                    if (winners != null && winners.Length > 0)
                    {
                        var users = await this.GetMappedUsers(winners, boardGame, channelSocket);

                        var response = $"Congrats {string.Join(", ", users)}!";
                        var embed = await boardGame.GetBoardGameEmbed();
                        await channelSocket.SendMessageAsync(response, false, embed);

                        await channelSocket.SendMessageAsync($"Removing {boardGame.GameState.GameName} from watch list...");
                        await this.RemoveWatch(boardGame.Service, boardGame.Game, boardGame.GameId, watch.ServerId, watch.ChannelId);
                    }

                    if (!oldCurrentPlayers.SequenceEqual(newCurrentPlayers))
                    {
                        var differentPlayers = newCurrentPlayers.Except(oldCurrentPlayers);
                        var users = await this.GetMappedUsers(differentPlayers, boardGame, channelSocket);
                        if (users.Count == 0) { return; }

                        var userList = string.Join(", ", users);
                        var response = $"It's the following players' turn in the game below: {userList}\n";

                        var embed = await boardGame.GetBoardGameEmbed();

                        await channelSocket.SendMessageAsync(response, false, embed);

                        watch.CurrentPlayers = newCurrentPlayers;
                        watch.ErrorCount = 0;
                        var filter = Builders<BoardGameWatchModel>.Filter.And(
                            Builders<BoardGameWatchModel>.Filter.Eq(x => x.BGKey, watch.BGKey),
                            Builders<BoardGameWatchModel>.Filter.Eq(x => x.ServerId, watch.ServerId),
                            Builders<BoardGameWatchModel>.Filter.Eq(x => x.ChannelId, watch.ChannelId));

                        await this.watchCollection.ReplaceOneAsync(filter, watch, new ReplaceOptions { IsUpsert = true });
                    }
                });
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error notifying channels");
            }
            finally
            {
                this.watchTimer.Enabled = true;
            }
        }

        private async Task<List<string>> GetMappedUsers(IEnumerable<string> players, BoardGame boardGame, ISocketMessageChannel channel)
        {
            var users = new List<string>();
            foreach (var player in players)
            {
                var mappedUser = boardGame.GetUserMapping(player);
                var discordUsers = await channel.GetUsersAsync().FlattenAsync();
                var user = discordUsers.FirstOrDefault(u => u.Username.ToLower().Equals(mappedUser?.ToLower() ?? player.ToLower()));
                users.Add(user?.Mention ?? player);
            }

            return users;
        }
    }
}
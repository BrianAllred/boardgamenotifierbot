using System;
using CommandLine;
using Serilog;

namespace BoardGameNotifier.Utilities
{
    public class Options
    {
        [Option("log-level", Default = Enums.LogLevel.Error, Required = false, HelpText = "Set log level (verbose, debug, information, warning, error, fatal)")]
        public Enums.LogLevel LogLevel { get; set; }

        [Option('t', "token", Required = false, HelpText = "Set the bot token")]
        public string Token { get; set; }

        [Option('i', "interval", Required = false, HelpText = "Set the polling interval")]
        public int PollInterval { get; set; }

        [Option("bgc-username", Required = false, HelpText = "Username for BoardGameCore")]
        public string BGCUsername { get; set; }

        [Option("bgc-password", Required = false, HelpText = "Password for BoardGameCore")]
        public string BGCPassword { get; set; }

        [Option("mongo-server", Default = "localhost", Required = false, HelpText = "MongoDB server address")]
        public string MongoServer { get; set; }

        [Option("mongo-port", Default = "27017", Required = false, HelpText = "MongoDB server port")]
        public string MongoPort { get; set; }

        [Option("mongo-db", Default = "bgn", Required = false, HelpText = "MongoDB database")]
        public string MongoDatabase { get; set; }

        [Option("mongo-username", Default = "root", Required = false, HelpText = "MongoDB username")]
        public string MongoUsername { get; set; }

        [Option("mongo-password", Default = "example", Required = false, HelpText = "MongoDB password")]
        public string MongoPassword { get; set; }

        public void Set()
        {
            this.Token = this.Token ?? Environment.GetEnvironmentVariable(Enums.EnvVar.BGN_BOT_TOKEN.ToString());
            this.BGCUsername = this.BGCUsername ?? Environment.GetEnvironmentVariable(Enums.EnvVar.BGN_BGC_USERNAME.ToString());
            this.BGCPassword = this.BGCPassword ?? Environment.GetEnvironmentVariable(Enums.EnvVar.BGN_BGC_PASSWORD.ToString());
            this.MongoServer = Environment.GetEnvironmentVariable(Enums.EnvVar.BGN_MONGO_SERVER.ToString()) ?? this.MongoServer;
            this.MongoPort = Environment.GetEnvironmentVariable(Enums.EnvVar.BGN_MONGO_PORT.ToString()) ?? this.MongoPort;
            this.MongoDatabase = Environment.GetEnvironmentVariable(Enums.EnvVar.BGN_MONGO_DATABASE.ToString()) ?? this.MongoDatabase;
            this.MongoUsername = Environment.GetEnvironmentVariable(Enums.EnvVar.BGN_MONGO_USERNAME.ToString()) ?? this.MongoUsername;
            this.MongoPassword = Environment.GetEnvironmentVariable(Enums.EnvVar.BGN_MONGO_PASSWORD.ToString()) ?? this.MongoPassword;

            int interval;
            if (this.PollInterval == 0)
            {
                this.PollInterval = int.TryParse(Environment.GetEnvironmentVariable(Enums.EnvVar.BGN_POLL_INTERVAL.ToString()), out interval) ? interval : 5000;
            }

            Enums.LogLevel logLevel;
            if (this.LogLevel == Enums.LogLevel.Error)
            {
                this.LogLevel = Enums.LogLevel.TryParse(Environment.GetEnvironmentVariable(Enums.EnvVar.BGN_LOG_LEVEL.ToString()), out logLevel) ? logLevel : Enums.LogLevel.Error;
            }
        }

        public string GetCurrentValues()
        {
            var message = "Current configuration:\n";
            var type = this.GetType();
            foreach (var property in type.GetProperties())
            {
                message += $"{property.Name}: {property.GetValue(this)}\n";
            }

            return message;
        }
    }
}
using System;
using System.Collections.Generic;
using OpenQA.Selenium.Chrome;

namespace BoardGameNotifier.Utilities
{
    public class ChromeDriverFactory
    {
        private readonly Dictionary<string, ChromeDriver> drivers = new Dictionary<string, ChromeDriver>();

        public ChromeDriver GetDriver(string key)
        {
            if (this.drivers.ContainsKey(key))
            {
                return this.drivers[key];
            }

            var driverOptions = new ChromeOptions();
            driverOptions.AddArgument("headless");
            driverOptions.AddArgument("whitelisted-ips");
            driverOptions.AddArgument("no-sandbox");
            driverOptions.AddArgument("disable-extensions");

            var driver = new ChromeDriver(driverOptions);
            driver.Manage().Timeouts().ImplicitWait = new TimeSpan(10 * TimeSpan.TicksPerSecond);
            this.drivers.Add(key, driver);
            return driver;
        }

        public bool RemoveDriver(string key)
        {
            if (!this.drivers.ContainsKey(key))
            {
                return false;
            }

            this.drivers[key].Dispose();
            return this.drivers.Remove(key);
        }
    }
}
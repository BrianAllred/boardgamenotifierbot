using System.Collections;

namespace BoardGameNotifier.Utilities
{
    public static class Helpers
    {
        public static int MatchingBraceIndex(this string s, int startingIndex)
        {
            var stack = new Stack();

            for (int i = startingIndex; i < s.Length; i++)
            {
                if (s[i] == '{')
                {
                    stack.Push((int)s[i]);
                }
                else if (s[i] == '}')
                {
                    stack.Pop();
                }

                if (stack.Count == 0)
                {
                    return i;
                }
            }

            return -1;
        }
    }
}
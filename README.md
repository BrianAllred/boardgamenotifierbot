# BoardGameNotifierBot

[![pipeline status](https://gitlab.com/BrianAllred/boardgamenotifierbot/badges/master/pipeline.svg)](https://gitlab.com/BrianAllred/boardgamenotifierbot/-/commits/master)

BoardGameNotifierBot is a Discord bot designed to work with BoardGameCore and BoardGameArena in order to notify players when it's their turn in a given game.

## Tested Games

Unlisted games are unknown. Work to support other games is on-going.

### BoardGameCore

|                     |     |        |
| ------------------- | --- | ------ |
| Food Chain Magnate  |     | Works  |
| Antiquity           |     | Broken |
| The Great Zimbabwe  |     | Works  |
| Wir sind das Volk ! |     | Broken |

**NOTE** that **all** BGC games **except** FCM require a login to be set (see below). They **do not** require an account that is participating in the game, any account will work. I recommend creating an account specifically for the bot.

### BoardGameArena

|           |     |       |
| --------- | --- | ----- |
| Keyflower |     | Works |

## Installation

**NOTE** BGNBot requires a MongoDB instance. By default, it uses connection string `mongodb://root:example@localhost:27017/bgn`. See Configuration below for more details.

1. Either download the latest artifact for your platform (Windows, Linux, or OSX) from [here](https://gitlab.com/BrianAllred/boardgamenotifierbot/-/tags) or run the Docker image (recommended) using the command

   `docker run -d --name BoardGameNotifier registry.gitlab.com/brianallred/boardgamenotifierbot:latest`

2. Set up a bot at the [Discord Developer Portal](https://discord.com/developers/applications).

3. Pass the bot token to the bot by using the environment variable `BGN_BOT_TOKEN` or using the `-t` parameter.

   - For Windows command prompt:

     ```bash
     set BGN_BOT_TOKEN=<token>
     BoardGameNotifier.exe
     ```

     ```bash
     BoardGameNotifier.exe -t <token>
     ```

   - For Linux/OSX:

     ```bash
     export BGN_BOT_TOKEN=<token>
     ./BoardGameNotifier
     ```

     ```bash
     ./BoardGameNotifier -t <token>
     ```

   - For Docker:

     ```bash
     docker run -d --name BoardGameNotifier --env BGN_BOT_TOKEN=<token> registry.gitlab.com/brianallred/boardgamenotifierbot:latest
     ```

     ```bash
     docker run -d --name BoardGameNotifier registry.gitlab.com/brianallred/boardgamenotifierbot:latest -t <token>
     ```

## Usage / Bot Commands

The bot allows different text channels to monitor different games, even within the same Discord server. So you can have different text channels for each game to make things easier to follow, or one text channel for all games so everyone can watch the fun.

Global commands are prefixed with `!bgn`.

BoardGameCore commands are prefixed with `!bgc`.

BoardGameArena commands are prefixed with `!bga`.

- `watch <service> <game> <gameId> <gameName> <userMappings>`  
   Watch a given game for turn changes.  
   `<service>` is used when using `!bgn` and should be either `bgc` or `bga`.  
   `<game>` and `<gameId>` come from the URL of the game.  
   `<gameName>` is used by `bga` to name games (since the service doesn't let you).  
   `<userMappings>` is a space separated list of BGC usernames and their corresponding Discord usernames, in case a user used different usernames for the two services. Optional.

- `removeWatch <service> <game> <gameId>`  
   Remove a given game from the watch list.  
   `<service>` is used when using `!bgn` and should be either `bgc` or `bga`.  
   `<game>` and `<gameId>` come from the URL of the game.

- `list`  
   List all watched games.

- `currentPlayers <service> <game> <gameId>`  
   List the current player(s) of the given game, or if game isn't given, list all games of the specified service, or all games of all services if using `bgn`.  
   `<service>` is used when using `!bgn` and should be either `bgc` or `bga`.  
   `<game>` and `<gameId>` come from the URL of the game and are optional.

- `help`  
   List commands for the given service.

## Configuration

`BGN_BOT_TOKEN` or `-t` or `--token` is required to be set.

`BGN_POLL_INTERVAL` or `-i` or `--interval` is the interval (in milliseconds) to poll BGC for turn changes. If not specified, default is 5 seconds.

`BGN_BGC_USERNAME` or `--bgc-username` is the username used to log in to BoardGameCore. Required for all BGC games except FCM.

`BGN_BGC_PASSWORD` or `--bgc-password` is the password used to log in to BoardGameCore. Required for all BGC games except FCM.

`BGN_LOG_LEVEL` or `--log-level` sets the logging level. Options are `Verbose, Debug, Information, Warning, Error, Fatal`. **NOTE that `Verbose` will leak the bot token and MongoDB username and password!** This is intended behavior to assist with debugging. Default level is `Error`.

`BGN_MONGO_SERVER` or `--mongo-server` is the address/hostname of the server hosting MongoDB. Default is `localhost`.

`BGN_MONGO_PORT` or `--mongo-port` is the port required to access MongoDB. Default is `27017`.

`BGN_MONGO_DATABASE` or `--mongo-db` is the name of the database in which to store BGN data. Default is `bgn`.

`BGN_MONGO_USERNAME` or `--mongo-username` is the username with access to the BGN database. Default is `root`.

`BGN_MONGO_PASSWORD` or `--mongo-password` is the password for the account above. Default is `example`.

### Docker Configuration

It's highly recommended to bind mount the host's `/dev/shm` to the container's `/dev/shm`. Due to the fact that each game watch is hosted in a separate Chrome tab, a standard container will run out of memory fairly quickly (3 or 4 game watches).

This can be accomplished by adding `-v /dev/shm:/dev/shm` to the `docker run` command. See the example docker-compose file for a docker-compose example.

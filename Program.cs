﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BoardGameNotifier.Discord;
using BoardGameNotifier.Utilities;
using CommandLine;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Serilog;
using Serilog.Exceptions;

namespace BoardGameNotifier
{
    public class Program
    {
        public static void Main(string[] args)
        {
            EstablishAsyncContext(args);
        }

        public static void EstablishAsyncContext(string[] args)
        {
            try
            {
                new Program().MainAsync(args).GetAwaiter().GetResult();

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Encountered error, restarting bot");

                EstablishAsyncContext(args);
            }
        }

        public async Task MainAsync(string[] args)
        {
            var options = new Options();

            var logConfig = new LoggerConfiguration()
                            .Enrich.WithExceptionDetails()
                            .WriteTo.Console(outputTemplate: Constants.LogTemplate);

            Parser.Default.ParseArguments<Options>(args)
                .WithParsed<Options>(opts =>
                {
                    options = opts;
                });

            options.Set();

            switch (options.LogLevel)
            {
                case Enums.LogLevel.Verbose:
                    logConfig.MinimumLevel.Verbose();
                    break;
                case Enums.LogLevel.Debug:
                    logConfig.MinimumLevel.Debug();
                    break;
                case Enums.LogLevel.Information:
                    logConfig.MinimumLevel.Information();
                    break;
                case Enums.LogLevel.Warning:
                    logConfig.MinimumLevel.Warning();
                    break;
                case Enums.LogLevel.Fatal:
                    logConfig.MinimumLevel.Fatal();
                    break;
                case Enums.LogLevel.Error:
                default:
                    logConfig.MinimumLevel.Error();
                    break;
            }

            Log.Logger = logConfig.CreateLogger();

            Log.Information("Set config values");
            Log.Verbose(options.GetCurrentValues());

            using (var services = ConfigureServices(options))
            {
                var watcher = services.GetRequiredService<BoardGameWatcher>();

                Log.Information("Setting up watcher");
                watcher.SetTimerInterval(options.PollInterval);

                Log.Information("Setting up Discord service logging");
                var client = services.GetRequiredService<DiscordSocketClient>();
                client.Log += LogMessage;
                services.GetRequiredService<CommandService>().Log += LogMessage;

                Log.Information("Logging into Discord and starting client");
                await client.LoginAsync(TokenType.Bot, options.Token);
                await client.StartAsync();

                client.Ready += async () =>
                {
                    await client.CurrentUser.ModifyAsync(user => user.Avatar = new Image(Constants.AvatarPath));
                    await client.SetGameAsync("!bgn help");
                };

                Log.Information("Initializing Discord services");
                await services.GetRequiredService<CommandHandler>().InitializeAsync();

                await Task.Delay(Timeout.Infinite);
            }
        }

        private Task LogMessage(LogMessage message)
        {
            switch (message.Severity)
            {
                case LogSeverity.Critical:
                    Log.Fatal(message.Message);
                    break;
                case LogSeverity.Warning:
                    Log.Warning(message.Message);
                    break;
                case LogSeverity.Info:
                    Log.Information(message.Message);
                    break;
                case LogSeverity.Verbose:
                    Log.Verbose(message.Message);
                    break;
                case LogSeverity.Debug:
                    Log.Debug(message.Message);
                    break;
                case LogSeverity.Error:
                default:
                    Log.Error(message.Message);
                    break;
            }

            return Task.CompletedTask;
        }

        private ServiceProvider ConfigureServices(Options options)
        {
            Log.Information("Configuring services");
            var mongoSettings = new MongoClientSettings();
            mongoSettings.Server = new MongoServerAddress(options.MongoServer, int.Parse(options.MongoPort));
            mongoSettings.Credential = MongoCredential.CreateCredential(options.MongoDatabase, options.MongoUsername, options.MongoPassword);
            var mongoClient = new MongoClient(mongoSettings);

            var collection = new ServiceCollection()
                .AddSingleton<Options>(options)
                .AddSingleton<MongoClient>(mongoClient)
                .AddSingleton<ChromeDriverFactory>();

            return collection.AddSingleton<DiscordSocketClient>()
                .AddSingleton<CommandService>()
                .AddSingleton<CommandHandler>()
                .AddSingleton<BoardGameWatcher>()
                .BuildServiceProvider();
        }
    }
}
